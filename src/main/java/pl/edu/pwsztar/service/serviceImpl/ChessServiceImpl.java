package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.service.ChessService;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.dto.Convert;
import pl.edu.pwsztar.domain.chess.Bishop;
import java.awt.Point;
import pl.edu.pwsztar.domain.enums.FigureType;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop;
    private Convert convert = new Convert();

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop) {
        this.bishop = bishop;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {

        // todo: przed przystapieniem do weryfikacji ruchy nalezy zaimplementowac konwerter pozycji figury
        // todo: dane przychodza w postaci zlepka znaku oraz cyfry np.: a_4, b_1 (informacja w logach)
        // todo: nalezy zaimplementowac klase ktora bedzie dokonywala konwersji znakow na wspolrzedne

        Point start = convert.stringToIndex(figureMoveDto.getStart());
        Point end = convert.stringToIndex(figureMoveDto.getDestination());

        // todo: nalezy zweryfikowac czy interfejs RulesOfGame nie posiada bledow - jakosc oprogramowania
        // todo: w przypadku wystapienie bledow nalezy zrefaktorowac interfejs

        boolean result = false;

        if(figureMoveDto.getType() == FigureType.BISHOP) {
            result = bishop.isCorrectMove(start, end);
        }

        // todo: nalezy rozpoznac typ figury oraz zwrocic informacje czy ruch jest dozwolony (boolean)
        return result;
    }
}

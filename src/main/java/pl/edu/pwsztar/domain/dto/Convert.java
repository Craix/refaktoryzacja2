package pl.edu.pwsztar.domain.dto;

import java.awt.Point;

public class Convert{

    public Point stringToIndex(String from)
    {
        Point result = new Point();

        result.x = from.charAt(0) - 96;
        result.y = from.charAt(2) - 48;

        return result;
    }
}
